# module for contacting webapp
import requests
import json

class Bouncer():
    def __init__(self):
        self.server = 'http://localhost:3000'

    def register_entry(self, card_uid_str):
        try:
            api_endpoint = '/api/passages'
            data={'card_uid': str(card_uid_str),
                  'passage_type': 'entry'}
            response = self.send_request(self.server+api_endpoint, data)
            print "server response: " + str(response)
            return response
        except:
            return False

    def register_exit(self, card_uid_str):
        try:
            api_endpoint = '/api/passages'
            data={'card_uid': str(card_uid_str),
                  'passage_type': 'exit'}
            response = self.send_request(self.server+api_endpoint, data)
            print "server response: " + str(response)
            return response
        except:
            return False

    def register_card(self, card_uid_str):
        try:
            api_endpoint = '/api/cards'
            data={'uid': str(card_uid_str)}
            return self.send_request(self.server+api_endpoint, data)
        except:
            return False

    def send_request(self, url, data):
        response = requests.post(url=url, data=data)
        if response.status_code == 201:
            content = json.loads(response.content)
            return content['authorized']
        else:
            return False
