#!/usr/bin/env python
# -*- coding: utf8 -*-


import RPi.GPIO as GPIO
import MRFC522Double
import signal
import time
from bouncer import Bouncer
from leds import Leds

read_time_gap    = 20 # Number of seconds to elapse before a particular NFC chip can be registered again

master_key = 13
continue_reading = True


# Capture SIGINT & SIGTERM for cleanup when the script is aborted
def EndRead(signal,frame):
    global continue_reading
    print "Interrupt caught, ending program."
    continue_reading = False

def TakeReading(MIFAREReader):
    device = None
    uid    = None

    # Scan for cards    
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        # Get the UID of the card
        (status,device, uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:

            # This is the default key for authentication
            key = [0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
            
            # Select the scanned tag
            MIFAREReader.MFRC522_SelectTag(uid)

            # Authenticate
            status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

            # Check if authenticated
            if status != MIFAREReader.MI_OK:
                device = None
                uid    = None

    return (device, uid)

def ConvertUIDListToString(uid):
    uid_string = ""
    
    if len(uid) > 3:
        uid_string = str(uid[0])+'-'+str(uid[1])+'-'+str(uid[2])+'-'+str(uid[3])

    return uid_string


# React if the user interrupts the script with Ctrl-C
signal.signal(signal.SIGINT, EndRead)
# React if our service is stopped
signal.signal(signal.SIGTERM, EndRead)

# Create an object of the class MFRC522 - this initialises GPIO
MIFAREReader = MRFC522Double.MRFC522Double(dev_list=['/dev/spidev0.0', '/dev/spidev0.1'])
# additional gpio setup for button
GPIO.setup(master_key,GPIO.IN, pull_up_down=GPIO.PUD_UP)


# Welcome message
print "Started tracking"

bouncer = Bouncer()
leds = Leds()
# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:
    master_value = GPIO.input(master_key)

    # See if any chips are ready to be read
    (device, uid) = TakeReading(MIFAREReader)

    if (uid is not None) and (type(uid) is list):
        uid_string = ConvertUIDListToString(uid)

        if  uid_string:
            if master_value == 0: # wcisnieto przycisk - dodaj nowe tagi
                bouncer.register_card(uid_string)
                leds.light_on_and_off()
            else:
                result = False
                print "device: " + str(device) + ", id: " + uid_string
                if device == 0:
                    result = bouncer.register_entry(uid_string)
                elif device == 1:
                    result = bouncer.register_exit(uid_string)
                if result:
                    print "ACCESS GRANTED"
                    leds.open()
                    leds.close()
                else:
                    print "You shall not pass!"
                    leds.blink()
                    
        else:
            print "Invalid UID: ("+str(device)+"); Card: "+uid_string

    # Switch to the next RFID reader
    MIFAREReader.MRFC522_AdvanceDevice()

# Finally clean everything up
GPIO.cleanup()