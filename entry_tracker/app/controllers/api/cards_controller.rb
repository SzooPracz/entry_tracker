module Api
  class CardsController < ApiController

    def create
      card = Card.new(uid: params[:uid])

      if card.save

        render json: card, status: :ok
      else
        render json: { errors: card.errors.messages }, status: :unprocessable_entity
      end
    end

    private

    def card_params
      params.permit(:uid, :authorized)
    end
  end
end
