module Api
  class PassagesController < ApiController

    def create
      passage = Passage.new(passage_params)

      passage.authorized = passage.card&.authorized?

      if passage.save
        render json: passage, status: :ok
      else
        render json: { errors: passage.errors.messages }, status: :unprocessable_entity
      end
    end

    private

    def passage_params
      params.permit(:passage_type, :card_uid)
    end
  end
end
