# frozen_string_literal: true

class Card < ApplicationRecord
  self.primary_key = :uid
  has_many :passages, foreign_key: :card_uid, inverse_of: :card

  validates :uid, presence: true, uniqueness: true
  validates :authorized, inclusion: { in: [true, false] }

  def to_s
    uid
  end
end
