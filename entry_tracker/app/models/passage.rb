class Passage < ApplicationRecord
  belongs_to :card, foreign_key: :card_uid, inverse_of: :passages

  enum passage_type: { exit: 0, entry: 1 }

  validates :card, presence: true
  validates :passage_type, presence: true
  validates :authorized, presence: true, inclusion: { in: [true, false] }

end
