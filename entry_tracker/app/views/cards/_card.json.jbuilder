json.extract! card, :id, :uid, :created_at, :updated_at
json.url card_url(card, format: :json)
