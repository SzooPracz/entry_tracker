json.extract! passage, :id, :authorized, :type, :card_id, :created_at, :updated_at
json.url passage_url(passage, format: :json)
