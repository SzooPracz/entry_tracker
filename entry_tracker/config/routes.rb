Rails.application.routes.draw do

  root to: 'home#index'
  resources :passages
  resources :cards do
    member do
      patch :grant_authorization
      patch :revoke_authorization
    end
  end

  namespace :api, defaults: {format: 'json'} do
    resources :cards, only: [:create]
    resources :passages, only: [:create]
  end

end
