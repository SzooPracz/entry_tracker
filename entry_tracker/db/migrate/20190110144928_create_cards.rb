class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards, id: false, primary_key: :uid do |t|
      t.string :uid, null: false
      t.boolean :authorized, null: false, default: true
      t.timestamps
    end
  end
end
