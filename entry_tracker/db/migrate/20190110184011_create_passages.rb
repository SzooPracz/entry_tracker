class CreatePassages < ActiveRecord::Migration[5.2]
  def change
    create_table :passages do |t|
      t.boolean :authorized, null: false, default: true
      t.integer :passage_type, null: false, default: 0
      t.string :card_uid, index: true
      t.timestamps
    end

    add_foreign_key :passages, :cards, column: :card_uid
  end
end
