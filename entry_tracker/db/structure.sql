CREATE TABLE "schema_migrations" ("version" varchar NOT NULL PRIMARY KEY);
CREATE TABLE "ar_internal_metadata" ("key" varchar NOT NULL PRIMARY KEY, "value" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "cards" ("uid" varchar NOT NULL, "authorized" boolean DEFAULT 1 NOT NULL, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE TABLE "passages" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "authorized" boolean DEFAULT 1 NOT NULL, "passage_type" integer DEFAULT 0 NOT NULL, "card_uid" varchar, "created_at" datetime NOT NULL, "updated_at" datetime NOT NULL);
CREATE INDEX "index_passages_on_card_uid" ON "passages" ("card_uid");
INSERT INTO "schema_migrations" (version) VALUES
('20190110144928'),
('20190110184011');


