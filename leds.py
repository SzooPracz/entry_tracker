import RPi.GPIO as GPIO
import time
from enum import Enum


class Leds():

    def __init__(self):
        self.blink_time = 0.1
        self.lightup_time = 0.3
        self.open_time = 1.0
        self.setup()

    def setup(self):
        self.leds = (29,31,33,35,37)
        for led in self.leds:
            GPIO.setup(led, GPIO.OUT)

    def open(self):
        for led in reversed(self.leds):
            GPIO.output(led, GPIO.HIGH)
            time.sleep(self.lightup_time)
        time.sleep(self.open_time)

    def close(self):
        for led in self.leds:
            GPIO.output(led, GPIO.LOW)
            time.sleep(self.lightup_time)

    def blink(self, n_times=5):
        for _ in range(n_times):
            for led in self.leds:
                GPIO.output(led, GPIO.HIGH)
            time.sleep(self.blink_time)
            for led in self.leds:
                GPIO.output(led, GPIO.LOW)
            time.sleep(self.blink_time)

    def light_on(self):
        for led in self.leds:
            GPIO.output(led, GPIO.HIGH)

    def light_off(self):
        for led in self.leds:
            GPIO.output(led, GPIO.LOW)

    def light_on_and_off(self):
        self.light_on()
        time.sleep(1)
        self.light_off()
        # GPIO.cleanup()
